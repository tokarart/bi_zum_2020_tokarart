from collections import deque
import heapq  # priority queue for dijkstra, greedy and a_star

from random import randrange

from additional_functions import print_labyrinth, restore_path


def get_neighbours(cell):
    x, y = cell
    return [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]


def random_search(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])
    q = []
    visited = {start: -1}
    q.append(start)
    while q:
        i = randrange(0, len(q))  # choose random open node
        curr = q[i]
        y_curr, x_curr = curr
        q.pop(i)

        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        if curr == end:  # target found
            path_nodes = restore_path(maze, visited, end)
            print_labyrinth(maze)
            return path_nodes, len(visited)

        for n in get_neighbours(curr):  # check all neighbours
            y, x = n
            if 0 <= x < height and 0 <= y < width and maze[x][y] != 'X' and n not in visited:
                q.append(n)
                visited[n] = curr

                # set as opened
                if maze[x][y] not in ['S', 'E']:
                    maze[x][y] = '0'

        if not no_animation:
            print_labyrinth(maze)

        # set as closed
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'


def bfs(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])

    q = deque()
    visited = {start: -1}  # map of source nodes
    q.append(start)
    while q:
        curr = q.popleft()
        y_curr, x_curr = curr

        # set as current
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        # target  found
        if curr == end:
            path_nodes = restore_path(maze, visited, end)
            print_labyrinth(maze)
            return path_nodes, len(visited)

        for n in get_neighbours(curr):  # check neighbors
            y, x = n
            if 0 <= x < height and 0 <= y < width and maze[x][y] != 'X' and n not in visited:
                q.append(n)
                visited[n] = curr

                # set as opened
                if maze[x][y] not in ['S', 'E']:
                    maze[x][y] = '0'

        if not no_animation:
            print_labyrinth(maze)

        # set as closed
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'


def dfs(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])

    q = []
    visited = {start: -1}  # map of source nodes
    q.append(start)

    while q:
        curr = q.pop()
        y_curr, x_curr = curr

        # set as current
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        # target  found
        if curr == end:
            path_nodes = restore_path(maze, visited, end)
            print_labyrinth(maze)
            return path_nodes, len(visited)

        # check neighbors
        for n in get_neighbours(curr):
            y, x = n
            if 0 <= x < height and 0 <= y < width and maze[x][y] != 'X' and n not in visited:
                q.append(n)
                visited[n] = curr

                # set as opened
                if maze[x][y] not in ['S', 'E']:
                    maze[x][y] = '0'

        if not no_animation:
            print_labyrinth(maze)

        # set as closed
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'


def dijkstra(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])

    q = []
    heapq.heappush(q, (0, start))
    parent = {start: -1}
    steps = {start: 0}

    while q:
        curr = q[0]
        y_curr, x_curr = curr[1]

        if curr[1] == end:  # target found
            restore_path(maze, parent, end)
            print_labyrinth(maze)
            return steps[end], len(steps)
        # set as current
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        curr_steps = curr[0]
        heapq.heappop(q)

        for n in get_neighbours(curr[1]):  # check neighbors
            y_n, x_n = n
            new_steps = curr_steps + 1
            if 0 <= x_n < height and 0 <= y_n < width and maze[x_n][y_n] != 'X' and \
                    (n not in steps or new_steps < steps[n]):
                steps[n] = new_steps
                heapq.heappush(q, (new_steps, n))
                parent[n] = curr[1]
                # set as opened
                if maze[x_n][y_n] not in ['S', 'E']:
                    maze[x_n][y_n] = '0'

        if not no_animation:
            print_labyrinth(maze)

        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'


def heuristic(pos, end):
    """
        Heuristic function for greedy search and a-star.
        In our case Manhattan distance guarantees that a-star finds shortest path
    """
    x_pos, y_pos = pos  # current node
    x_end, y_end = end  # target node
    return abs(x_pos - x_end) + abs(y_pos - y_end)


def a_star(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])

    q = []
    heapq.heappush(q, (0, start))  # priority queue
    parent = {start: -1}  # dictionary of parent nodes
    cost = {start: (0, 0)}  # dictionary of costs in visited nodes
    while q:
        curr = q[0]
        curr_cost = cost[curr[1]]
        heapq.heappop(q)
        y_curr, x_curr = curr[1]

        # set as current
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        if curr[1] == end:  # target found
            path_nodes = restore_path(maze, parent, end)
            print_labyrinth(maze)
            return path_nodes, len(cost)

        for n in get_neighbours(curr[1]):
            y_n = n[0]
            x_n = n[1]
            g = curr_cost[0] + 1  # cost of one step is 1
            h = heuristic(n, end)
            f = g + h
            if 0 <= x_n < height and 0 <= y_n < width and maze[x_n][y_n] != 'X' and \
                    (n not in cost or curr_cost[1] > f):
                cost[n] = (g, f)
                heapq.heappush(q, (f, n))
                parent[n] = curr[1]

                # set as opened
                if maze[x_n][y_n] not in ['S', 'E']:
                    maze[x_n][y_n] = '0'

        if not no_animation:
            print_labyrinth(maze)

        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'


def greedy(maze, start, end, no_animation=False):
    height = len(maze)
    width = len(maze[0])

    q = []
    heapq.heappush(q, (0, start))
    parent = {start: -1}
    cost = {start: heuristic(start, end)}

    while q:
        curr = q[0]
        heapq.heappop(q)
        y_curr, x_curr = curr[1]

        # set as current
        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = 'C'

        if curr[1] == end:
            i = restore_path(maze, parent, end)
            print_labyrinth(maze)
            return i, len(cost)

        for n in get_neighbours(curr[1]):
            y_n = n[0]
            x_n = n[1]
            new_h = heuristic(n, end)

            if 0 <= x_n < height and 0 <= y_n < width and maze[x_n][y_n] != 'X' and n not in cost:
                cost[n] = new_h
                heapq.heappush(q, (new_h, n))
                parent[n] = curr[1]

                # set as opened
                if maze[x_n][y_n] not in ['S', 'E']:
                    maze[x_n][y_n] = '0'

        if not no_animation:
            print_labyrinth(maze)

        if maze[x_curr][y_curr] not in ['S', 'E']:
            maze[x_curr][y_curr] = '#'
