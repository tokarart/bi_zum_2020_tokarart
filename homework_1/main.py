import os
from additional_functions import read_map
from algs import *


def menu():
    files = dict(zip(range(1, len(os.listdir("test")) + 1), os.listdir("test")))
    algorithms = {
        1: random_search,
        2: bfs,
        3: dfs,
        4: dijkstra,
        5: greedy,
        6: a_star,
    }
    file_choice = -1

    while file_choice not in files.keys():
        print("Choose the file with your maze.")
        for k, v in files.items():
            print(k, v)
        file_choice = int(input())
        print()
    print('file {} choosen'.format(files[file_choice]))
    alg_choice = -1

    while alg_choice not in algorithms.keys():
        print("Choose the algorithm")
        for k, v in algorithms.items():
            print(k, v.__name__)
        alg_choice = int(input())
        print()

    animation = -1
    while animation not in [0, 1]:
        print("Animation?\n\n  0: With animation\n  1: Only result\n")
        animation = int(input())
        print()
    maze, start, end = read_map('./test/' + files[file_choice])
    path, expanded = algorithms[alg_choice](maze, start, end, animation)
    print('nodes expanded: {}\npath length: {}'.format(expanded, path))


menu()
