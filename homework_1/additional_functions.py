from time import sleep
from termcolor import colored

import os


def read_map(file):
    """
    function transforms file into array of chars and returns it
    """
    file = open(file, 'r')
    lines = file.readlines()
    maze = list()
    for i, line in enumerate(lines):
        if 'start' in line:
            start = line.replace('start ', '')
            start = start[0:-1]
            start = tuple(int(i) for i in start.split(','))
        elif 'end' in line:
            end = line.replace('end', '')
            end = end[0:-1]
            end = tuple(int(i) for i in end.split(','))
        else:
            res = list(line)
            res = res[0: -1]
            maze.append(res)
    maze[int(start[1])][int(start[0])] = 'S'
    maze[int(end[1])][int(end[0])] = 'E'
    return maze, start, end


def print_labyrinth(l):
    sleep_and_clean()
    l = ["".join(row) for row in l]
    l = "\n".join(l)
    for c in l:
        if c == '\n':
            print('\n', end='', sep='')
        elif c == 'X':
            print(colored('X', 'white'), end='', sep='')
        elif c == '#':
            print(colored('#', 'red'), end='', sep='')
        elif c == '0':
            print(colored('0', 'yellow'), end='', sep='')
        elif c == 'o':
            print(colored('o', 'green'), end='', sep='')
        elif c == ' ':
            print(' ', end='', sep='')
        elif c == 'S':
            print(colored('S', 'magenta'), end='', sep='')
        elif c == 'E':
            print(colored('E', 'magenta'), end='', sep='')
        elif c == 'C':
            print(colored('C', 'blue'), end='', sep='')
    print()


def sleep_and_clean(time=0.05):
    sleep(time)
    print(chr(27) + "[2J")


def restore_path(maze, visited, end):
    i = 0
    curr = end
    while curr != -1:  # source node has -1 as a parent
        y = curr[0]
        x = curr[1]
        if curr == end:
            maze[x][y] = 'E'
        elif visited[curr] == -1:
            maze[x][y] = 'S'
        else:
            maze[x][y] = 'o'
        curr = visited[curr]
        i += 1
    return i - 1
