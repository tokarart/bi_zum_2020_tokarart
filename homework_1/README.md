# Prohledavani stavoveho prostoru

To je prvni pocitacova uoha z predmetu BI-ZUM (B192). Implementuje 6 ruznych algoritmu prohledavani stavoveho prostoru (Random search, BFS, DFS, Dijkstruv algoritmus, Greedy search and A-Star algorithm). 

## Navod na spusteni

1. Aktivujite virtualni prostredi
```bash
. venv/bin/activate
```
2.Spustte main.py
```python
python main.py
```
 