(define (problem sailor_problem)
    (:domain sailor_domain)
    
    (:objects forest river port pub town lighthouse academy sea island sober tipsy drunk addicted hardened boat frigate caravel
		sailor pirate capitan admiral happy_husband happy_admiral happy_addicted
        wood coconuts flowers doubtful_connections good_connections smuggler_connections criminal_record golden_grains golden_coin 
        golden_brick bear_leather victory_over_bear victory_over_pirates alcohol treasure_map pearl cocaine ring  girlfriend
    )

    (:init 
        (ground_path pub port)
		(ground_path port pub)
		(ground_path academy town)
		(ground_path town academy)
		(ground_path forest river)
		(ground_path river forest)
		(ground_path river port)
		(ground_path port river)
		(ground_path port town)
		(ground_path town port)
		(sea_path port lighthouse)
		(sea_path lighthouse port)
		(sea_path port sea)
		(sea_path sea port)
		(sea_path sea lighthouse)
		(sea_path lighthouse sea)
		(sea_path sea island)
		(sea_path island sea)
		(sailor_on port)
		(is sailor)
		(in_condition sober)
    )

    (:goal (and
        (is happy_husband)
		(is happy_admiral)
		(is happy_addicted)
    ))
)