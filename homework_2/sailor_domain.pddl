(define (domain sailor_domain)
    (:requirements :negative-preconditions)

    (:predicates
        (ground_path ?l1 ?l2)
        (sea_path ?l1 ?l2)
        (has ?object)
        (is ?status)
        (in_condition ?condition)
        (sailor_on ?location)
    )

    
    (:action walk
        :parameters (?from ?to)
        :precondition (and 
            (ground_path ?from ?to) 
            (sailor_on ?from))
        :effect (and 
            (not (sailor_on ?from)) 
            (sailor_on ?to)
        )
    )
    
    (:action create_boat
        :parameters ()
        :precondition (and
            (has wood)
        )
        :effect (and 
            (has boat)
            (not (has wood))
        )
    
    )
    
    (:action create_frigate
        :parameters()
        :precondition(and 
            (has boat)
            (has wood)
            (has golden_grains)
        )
        :effect(and
            (has frigate)
            (not (has wood))
            (not (has boat))
            (not (has golden_grains))
        )
        
    )
    
    (:action create_caravel
        :parameters()
        :precondition(and 
            (has boat)
            (has wood)
            (has golden_coin)
        )
        :effect(and
            (has caravel)
            (not (has wood))
            (not (has boat))
            (not (has golden_coin))
        )
        
    )
    
    (:action sail_on_boat
        :parameters (?from ?to)
        :precondition(and
            (has boat)
            (sailor_on ?from)
            (sea_path ?from ?to)
        )
        :effect(and
            (sailor_on ?to)
            (not (sailor_on ?from))
        )
    )
    
    (:action sail_on_frigate
        :parameters (?from ?to)
        :precondition(and
            (has frigate)
            (sailor_on ?from)
            (sea_path ?from ?to)
        )
        :effect(and
            (sailor_on ?to)
            (not (sailor_on ?from))
        )
    )

    (:action sail_on_caravel
        :parameters (?from ?to)
        :precondition(and
            (has caravel)
            (sailor_on ?from)
            (sea_path ?from ?to)
        )
        :effect(and
            (sailor_on ?to)
            (not (sailor_on ?from))
        )
    )
    
    
    (:action get_tipsy
        :parameters ()
        :precondition (and 
            (in_condition sober)
            (has alcohol)
        )
        :effect (and 
            (in_condition tipsy)
            (not (in_condition sober))
            (not (has alcohol))
        )    
    )
    
    (:action get_drunk
        :parameters ()
        :precondition (and 
            (in_condition tipsy)
            (has alcohol)
        )
        :effect (and
            (not (in_condition tipsy)) 
            (in_condition drunk)
            (not (has alcohol))
        )    
    )
    
    (:action get_addicted
        :parameters ()
        :precondition (and 
            (in_condition drunk)
            (has alcohol)
        )
        :effect (and 
            (not (in_condition drunk))
            (in_condition addicted)
            (not (has alcohol))
        )    
    )
    

    (:action cut_down_wood_in_forest
        :parameters()
        :precondition(and
            (sailor_on forest)
        )
        :effect(and
            (has wood)
        )
    )
    
    (:action pick_flowers
        :parameters()
        :precondition(and
            (sailor_on forest)
        )
        :effect(and
            (has flowers)
        )
    )
    
    (:action fight_the_bear
        :parameters()
        :precondition(and
            (sailor_on forest)
        )
        :effect(and
            (has bear_leather)
            (in_condition hardened)
            (has victory_over_bear)
        )
    )
    
    (:action meet_wizard
        :parameters()
        :precondition(and
            (sailor_on forest)
            (has alcohol)
        )
        :effect(and
            (not (has alcohol))
            (has doubtful_connections)
            (has treasure_map)
        )
    )
    
    (:action steal_boat
        :parameters()
        :precondition(and
            (sailor_on river)
        )
        :effect(and
            (has boat)
            (has criminal_record)
        )
    )
    
    (:action pan_gold
        :parameters()
        :precondition(and
            (sailor_on river)
            (not (has golden_grains))
        )
        :effect(and
            (has golden_grains)
        )
    )
    
    (:action take_ice_bath_on_river
        :parameters()
        :precondition(and
            (not (in_condition sober))
            (sailor_on river)
        )
        :effect(and
            (not(in_condition tipsy))
            (not(in_condition drunk))
            (in_condition sober)
        )
    )
    
    (:action work
        :parameters()
        :precondition(and
            (sailor_on port)
            (not (has golden_grains))
        )
        :effect(and
            (has golden_grains)
        )
    )
    
    (:action sell_coconuts
        :parameters()
        :precondition(and
            (sailor_on port)
            (has coconuts)
            (not (has golden_coin))

        )
        :effect(and
            (not(has coconuts))
            (has golden_coin)
        )
    )
    
    (:action sell_bear_leather
        :parameters()
        :precondition(and
            (sailor_on port)
            (has bear_leather)
            (not (has golden_coin))
        )
        :effect(and
            (not(has bear_leather))
            (has golden_coin)
        )
    )
    
    (:action meet_smugglers
        :parameters()
        :precondition(and
            (sailor_on port)
            (has doubtful_connections)
            (has golden_brick)
        )
        :effect(and
            (has smuggler_connections)
        )
    )
    
    (:action buy_alcohol
        :parameters()
        :precondition(and
            (sailor_on pub)
            (has golden_grains)
            (not (has alcohol))
        )
        :effect(and
            (has alcohol)
            (not (has golden_grains))
        )
    )
    
    (:action pay_round_whole_pub
        :parameters()
        :precondition(and
            (sailor_on pub)
            (has golden_coin)
        )
        :effect(and
            (not (has golden_coin))
            (has good_connections)
        )
    )
    
    (:action harden_in_battle
        :parameters()
        :precondition(and
            (sailor_on pub)
            (in_condition tipsy)
        )
        :effect(and
            (in_condition hardened)
        )   
    )
    
    (:action accumuate
        :parameters()
        :precondition(and
            (has golden_grains)
            (sailor_on town)
        )
        :effect(and
            (has golden_coin)
            (not (has golden_grains))
            (has good_connections)
        )
    )
    
    (:action invest
        :parameters()
        :precondition(and
            (has golden_coin)
            (sailor_on town)
        )
        :effect(and
            (has golden_brick)
            (not (has golden_coin))
            (has good_connections)
        )
    )
    
    (:action become_an_evil
        :parameters()
        :precondition(and
            (sailor_on town)
        )
        :effect(and
            (has golden_coin)
            (has criminal_record)
        )
    )
    
    (:action buy_indulgence
        :parameters()
        :precondition(and
            (sailor_on town)
            (has golden_grains)
            (has criminal_record)
        )
        :effect(and
            (not (has golden_grains))
            (not (has criminal_record))
        )
    )
    
    (:action work_community_service
        :parameters()
        :precondition(and 
            (sailor_on town)
            (has criminal_record)
            (in_condition sober)
        )
        :effect(and
            (not (in_condition sober))
            (not (has criminal_record))
            (in_condition tipsy)
        )
    )
    
    (:action become_capitan
        :parameters()
        :precondition(and
            (sailor_on academy)
            (not (has criminal_record))
            (has golden_coin)
            (is sailor)
        )
        :effect(and
            (not (has golden_coin))
            (is capitan)
        )
    )
    
    (:action meet_pirates_and_lose_everything
        :parameters()
        :precondition(and
            (sailor_on sea)
            (not (in_condition hardened))
        )
        :effect(and
            (in_condition hardened)
            (not (has golden_grains))
            (not (has golden_coin))
            (not (has golden_brick))
            (not (has caravel))
            (not (has frigate))
        )
    )
    
    (:action meet_pirates_and_join_them
        :parameters()
        :precondition(and
            (sailor_on sea)
            (has doubtful_connections)
            (not (is pirate))
        )
        :effect(and
            (not (in_condition sober))
            (in_condition tipsy)
            (is pirate)
        )
    )
    
    (:action meet_pirates_beat_them
        :parameters()
        :precondition(and
            (sailor_on sea)
            (in_condition hardened)
            (has caravel)
        )
        :effect(and
            (has golden_grains)
            (has golden_coin)
            (has golden_brick)
            (has caravel)
            (has frigate)
            (has victory_over_pirates)
        )
    )
    
    (:action dive_for_pearls
        :parameters()
        :precondition(and
            (sailor_on sea)
        )
        :effect(and
            (has pearl)
        )
    )
    
    (:action take_ice_bath_on_sea
        :parameters()
        :precondition (and
            (sailor_on sea)
            (not (in_condition sober))
        )
        :effect(and
            (not(in_condition tipsy))
            (not(in_condition drunk))
            (in_condition sober)
        )
    )
    
    (:action charm_girl_thanks_to_bear
        :parameters()
        :precondition(and
            (sailor_on lighthouse)
            (has victory_over_bear)
        )
        :effect(and
            (has girlfriend)
        )
    )
    
    (:action charm_girl_thanks_to_pirates
        :parameters()
        :precondition(and
            (sailor_on lighthouse)
            (has victory_over_pirates)
        )
        :effect(and
            (has girlfriend)
        )
    )
    
    (:action charm_girl_being_capitan
        :parameters()
        :precondition(and
            (sailor_on lighthouse)
            (is capitan)
        )
        :effect(and
            (has girlfriend)
        )
    )
    
    (:action collect_coconuts
        :parameters()
        :precondition(and
            (sailor_on island)
        )
        :effect(and
            (has coconuts)
        )
    )
    
    (:action cut_down_wood_on_island
        :parameters()
        :precondition(and
            (sailor_on island)
        )
        :effect(and
            (has wood)
        )
    )
    
    (:action find_cocaine
        :parameters()
        :precondition(and
            (sailor_on island)
            (has treasure_map)
        )
        :effect(and
            (has cocaine)
        )
    )

    (:action make_a_ring
        :parameters ()
        :precondition (and
            (has golden_brick)
            (has pearl)
        )
        :effect (and
            (has ring)
            (not (has golden_brick))
            (not (has pearl))
        )
    )

    (:action get_married
        :parameters ()
        :precondition (and
            (has girlfriend)
            (has ring)
            (has flowers)
            (has good_connections)
            (not (has criminal_record))
            (sailor_on island)
            (not (in_condition drunk))
            (not (in_condition addicted))
        )
        :effect (and
            (is happy_husband)
            (not (has ring))
            (not (has flowers))
        )
    )

    (:action become_admiral
        :parameters ()
        :precondition (and
            (is capitan)
            (has victory_over_pirates)
            (sailor_on academy)
            (in_condition sober)
        )
        :effect (and
            (is admiral)
            (is happy_admiral)
        )
    )
    
    (:action cocaine
        :parameters ()
        :precondition (and
            (has cocaine)
            (in_condition addicted)
            (has frigate)
            (has smuggler_connections)
            (has golden_brick)
        )
        :effect (and
            (is happy_addicted)
        )
    )   
)