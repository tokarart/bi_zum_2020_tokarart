import os
import cv2
from OCR import OCR
import matplotlib.pyplot as plt

def read_images(path):
    """
    function reads all images in given path
    returns list oh images (np.array) and list of filenames without file format
    """
    images = []
    letters = []
    paths = sorted(os.listdir(path))
    for p in paths:
        img = cv2.imread(path + '/' + p)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # convert to gray scale
        
        _, img = cv2.threshold(img, 125, 255, cv2.THRESH_BINARY)
        
        images.append(img)
        letters.append(p.split('.')[0])
    return images, letters


def main():
    X, y = read_images("./written_hiragana_dataset")
    ocr = OCR(len(y), X[0].shape, 500, 1000)
    ocr.fit(X, y, extended_pixels=False, enabled_taboo=True, taboo_size=200, initialization_mode='Distributed', logs=True   )
    a = [ocr.predict(img) for img in X]  # get predictions on given dataset
    assert a == y, 'Predictions are incorrect'   # check if predictions are correct


if __name__ == "__main__":
    main()
