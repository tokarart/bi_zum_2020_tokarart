from math import log2, ceil
from random import randint, choice, shuffle
import matplotlib.pylab as plt
from itertools import cycle
from collections import deque


class OCR(object):
    def __init__(self, n_letters: int, shape: tuple, max_runs: int, max_iter: int):
        self.shape = shape  # shape of pictures in dataset
        self.n_letters = n_letters  # amount of images
        self.max_runs = max_runs  # max restarts
        self.max_iter = max_iter  # max amount of iterations in one run
        self.images = []  # images used
        self.image_dict = {}  # dictionary for image prediction, where key is combination of pixel values for this image
        self.pixels = []  # pixes used to distinguis images

    def fit(self, images, letters, logs=False, extended_pixels=False, initialization_mode='Distributed',
            enabled_taboo=True, taboo_size=100):
        """
        method runs hill climbing algorithm on given set of images
        """
        self.images = images
        self.pixels = self.define_pixels(logs=logs, initialization_mode=initialization_mode,
                                         extended_pixels=extended_pixels, enable_taboo=enabled_taboo,
                                         taboo_size=taboo_size)
        for img, letter in zip(self.images, letters):  # fill dictionary with pixel values for each image
            self.image_dict[self.get_pixels_values(img, self.pixels)] = letter

    def predict(self, image):
        """
        method predicts which letter is in image
        """
        return self.image_dict[self.get_pixels_values(image, self.pixels)]

    def init_pixels(self, n: int, mode='Random'):
        """
        method initializes n random pixels in two modes:
            Random: pixels randomly initialized on whole picture
            Distributed: pixels are distributed evenly between quarters of picture
        """
        pixels = []
        if mode == 'Random':
            for i in range(n):
                while True:
                    pixel = (randint(0, self.shape[0] - 1), randint(0, self.shape[0] - 1))
                    if pixel not in pixels:
                        break
                pixels.append(pixel)
        if mode == 'Distributed':
            mid_x, max_x = ceil(self.shape[0] / 2 - 1), self.shape[0] - 1
            mid_y, max_y = ceil(self.shape[1] / 2 - 1), self.shape[1] - 1
            parts_dict = {  # dictionary of ranges for each quarter
                0: (0, mid_x, 0, mid_y),
                1: (mid_x + 1, max_x, 0, mid_y),
                2: (mid_x + 1, max_x, mid_y + 1, max_y),
                3: (0, mid_y, mid_y + 1, max_y)
            }
            quarters = [0, 1, 2, 3]
            quarter = cycle(quarters)
            for i in range(n):
                while True:
                    curr_qtr = next(quarter)
                    x_b, x_t, y_b, y_t = parts_dict[curr_qtr]
                    pixel = (randint(x_b, x_t), randint(y_b, y_t))
                    if pixel not in pixels:
                        break
                pixels.append(pixel)
        return pixels

    def get_pixels_values(self, image, pixels):
        """
        method returns tuple of pixels values on given image
        """
        return tuple(image[pixel] for pixel in pixels)

    def check_pixel(self, pixel):
        """
        returns true if pixel not crossing image bounds
        """
        x, y = pixel
        return 0 <= x < 16 and 0 <= y < 16

    def get_pixel_neighbor(self, pixel):
        """
        method returns 8 neighbours for given pixel
        """
        x, y = pixel
        res = [(x - 1, y), (x, y - 1), (x + 1, y), (x, y + 1), (x + 1, y + 1), (x - 1, y - 1), (x + 1, y - 1),
               (x - 1, y + 1)]
        return [pixel for pixel in res if self.check_pixel(pixel)]

    def get_possible_steps(self, pixels):
        """
        method returns list of possible steps from given position, where one step is move by one pixel
        """
        neighbors = []
        for i in range(len(pixels)):
            pixel_neighbors = self.get_pixel_neighbor(pixels[i])
            for pixel_nbr in pixel_neighbors:
                if pixel_nbr not in pixels:  # check if neighbor pixel is already in pixels
                    tmp = list(pixel for idx, pixel in enumerate(pixels) if idx != i)
                    tmp.append(pixel_nbr)
                    neighbors.append(tmp)
        return neighbors

    def evaluate_performance(self, pixels):
        """
        for given set of pixels counts how many unique combinations of values theese pixel have on given dataset
        """
        res = set()
        for image in self.images:
            res.add(self.get_pixels_values(image, pixels))
        return len(res)

    def define_pixels(self, logs: bool, initialization_mode: str, extended_pixels: bool, enable_taboo: bool,
                      taboo_size: int):
        """
        method returns set of pixels with maximum performance reached
        """
        iter_res = {}
        runs_res = {}
        n_pixels = ceil(log2(self.n_letters))  # minimal amount needed to distinguish images from given dataset

        if extended_pixels:
            n_pixels += 1

        for run in range(self.max_runs):
            pixels = self.init_pixels(n_pixels, mode=initialization_mode)  # pixel initialization
            performance = self.evaluate_performance(
                pixels)  # how much images are distinguished with this pixels combiantion
            iteration = 0
            taboo = deque(taboo_size * [None], taboo_size)  # taboo container

            while self.evaluate_performance(pixels) != self.n_letters and iteration <= self.max_iter:
                neighbors = self.get_possible_steps(pixels)  # list of possible moves
                if enable_taboo:  # delete tabooed moves
                    neighbors = [neighbor for neighbor in neighbors if neighbor not in taboo]
                perf_nbr = dict()  # dict to keep performance of possible moves
                for nbr in neighbors:
                    tmp_prf = self.evaluate_performance(nbr)
                    if tmp_prf not in perf_nbr.keys():
                        perf_nbr[tmp_prf] = []
                    perf_nbr[tmp_prf].append(nbr)

                best_prf = max(perf_nbr.keys())  # choose best performance
                best_nbr = choice(perf_nbr[best_prf])  # take random neighbor with best performance on this iteration

                if best_prf > performance:  # found better combination
                    if logs:
                        print(f'Found better position with {best_prf} unique combinations instead {performance}')
                    performance = best_prf
                    pixels = best_nbr

                else:  # couldn't find better combinations choose random one
                    pixels = choice(neighbors)
                    performance = self.evaluate_performance(pixels)

                if enable_taboo:  # add current position to taboo, if enabled
                    taboo.append(pixels)
                iter_res[performance] = pixels  # remember iteration result

                iteration += 1

            best_performance_iteration = max(iter_res.keys())  # find best performance during one run
            runs_res[best_performance_iteration] = iter_res[best_performance_iteration]  # get best set of pixels in run

            if performance == self.n_letters:  # needed combination found
                if logs:
                    print(f'Solution found')
                break

        best_combination = runs_res[max(runs_res.keys())]  # get best set of pixels among all runs
        print(f"Best opportunity is {best_combination} with performance {max(runs_res.keys())}")
        return best_combination
