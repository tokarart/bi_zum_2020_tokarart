# BI-ZUM Semestralni uloha

## Navod na spusteni
Naklonujte si repozitar
```bash
git clone git@gitlab.fit.cvut.cz:tokarart/bi_zum_2020_tokarart.git
cd bi_zum_2020_tokarart/semester_work
```
Udelejte virtualni prostredi
```bash
python -m venv venv
source venv/bin/activate
```
Nainstalujte potrebne knihovny
```bash
pip install -r requirements.txt
```
Spustte aplikaci
```bash
python main.py
```

Poznamka: Argumentem funkce read_images je adresar s datasetem

## Zadani
Jako semestralni ulohu jsem zvolil tema OCR. Ukolem je najit pozice nekolika pixelu tak, aby bylo mozne podle hodnot techto pixelu znaky abecedy od sebe rozeznat.
## Postup 
Jako zaklad reseni jsem zvolil algorimus Stochastic Hill Climbing s restarty. 
Na zacatku kazdeho spusteni jsem nahodne inicializoval ceil(log2(#POCET_PISMEN)) pixelu a pak spoustel algoritmus tak, ze jeden pohyb je pohnuti jednim pixelem. 
Kriterialni funkce F (v kodu calculate_performance) pro danou n-tici pixelu vrati pocet unikatnich kombinaci hodnot pixelu pro vsechny obrazky. 
Ze vsech sousedu vybiram ty nejlepsi (s nejvetsi hodnotou F(soused)) a z ne nahodne zvolim jeden (proto v nazvu slovo **Stochastic**).  

Tento postup je funkcni, ale pro dataset s anglickou abecedou algoritmus muze bezet do
 nekolika minut. Na datasetu s japonskymi pismeny to fungovalo docela rychle (radove stovky milisekund).
 
 Cilem nasledujicich experimentu je zrychleni vypoctu pro anglickou abecedu.
 
### Napad 1: Pouziti vetsiho mozstvi pixelu pro rozeznavani pismen
 Pro anglickou abecedu by melo stacit 5 pixelu, ale proc zrovna neszkusit pouzit vetsi mnozstvi, treba ceil(log2(#POCET_PISMEN)) + 1
 Tato strategie vyrazne zrychluje vypocet a pro anglickou abecedu potrebna sestice se nalezne radove za sekundy nebo mene.
 

Pokud to chcete zkusit v kodu, tak metoda OCR.fit() ma argument **extended_pixels** defaultne nastaveny na False,
 staci tu metodu zavolat nasledujicim zpusobem
```python
ocr.fit(X, y, extended_pixels=True)
``` 

Co kdyz chceme nechat minimalni pocet pixelu pro rozliseni?
 
### Napad 2: Rovnomerna inicializace pixelu
Pokud inicializujeme pozice pixelu nahodne, tak se muze nastat situace, ze nekolik (anebo vsechny)
pixely budou blizko od sebe, coz algoritmu docela vadi. 
Vyzkousel jsem rozdelit obrazky na 4 stejne velike oblasti a postupne pridavat 
po jednom pixelu do kazde oblasti. Tim jsem dostal potrebny pocet pixelu distribuovanych po celem obrazku. 

![Pixels distribution exaple](./plots/init_modes.png)

Diky tomu by se mel vypocet zrychlit, ale neda se to nejak rozume overit u stochastickeho algoritmu. 
Zkusil jsem 10 krat zpustit algoritmus ve dvou ruznych modech:

![10 runs in different modes](./plots/Random_vs_Distributed.png)

V prumeru:

![Mean runtime for different initialization modes](./plots/Random_vs_distributed_mean.png)

Podle vysledku je videt, ze distriucni mod pri inicializaci vede na sparvny vysledek rychleji, nez uple nahodna inicializace pozic pixelu.
 V nasledujicich experimentech bude pouzita distribucni metoda inicializace.
   
Aby algoritmus pouzil distribuovanou inicializaci pixelu, staci zavolat metodu fit nasledujicim zpusobem
```python
ocr.fit(X, y, initialization_mode='Distributed') 
```
### Napad 3: Tabu prohledavani
Pokud zakazeme stoupat na prozkoumane stavy v nejake nedavne dobe (treba 50 poslednich iteraci), 
tak bychom se mohli vyhnout uvaznuti na lokalnich extremech a jeste zrychlit hledani potrebne sekvenci pixelu. 
Ale kolik stavu mame v tabu chranit. Zkousel jsem zpustit algoritmus 10 krat s ruznymi velikostmi tabu:

![Different taboo sizes](./plots/taboosize.png)

Je videt, ze nelepsi volbou bude velikost 100 (pro limit iteraci v 500), takze zhruba 1/5 max poctu iteraci. 
Take vidime, ze doba behu v prumeru klesla ze 100 sekund (viz. runtime v predchozim bode) na 30. 

Nasledujici graf ukazuje doby behu 10 spusteni s velikosti tabu 100

![best taboo size](./plots/runtime_taboo_100.png)

Aby algoritmus pouzil tabu prohledavani, staci metodu fit zavolat nasledujicim zpusobem:
```python
ocr.fit(X, y, enabled_taboo=True, taboo_size=100)
```

### Napad 4: Najit nejlepsi vztah mezi max_runs a max_iter
Co bude vhodnejsi, vic restartu a min iteraci nebo naopak? Zkusil jsem zpustit algoritmus 10 krat s ruznymi konfiguracemi:

![average tuntime with different configs](./plots/average_runtime_cnfiguration.png)

Podle toho grafu by se mohlo rict, ze optimalnejsi bude vetsi pocet restartu s malym mnozstvim iteraci, 
ale pro absolutne opacne situace (1000/250 vs 250/1000) vysledek je skoro stejny (29 vs. 25 sekund). Jedine co muzeme rict je to, ze 
vztah poctu restaru k poctu iteraci by mel byt v rozsahu <0.25, 4>.

![best config](./plots/best_config_runtime.png)

Detalnejsim pohledem zjistime, ze parkrat algoritmus skoncil velmi rychle, coz je vysledkem vhodne inicializace.

## Zaver 
Dokazal jsem zrychlit vypocet pro dataset s anglickou abecedou pomoci ditribuovane inicializace pixelu a 
metody vyvezeni z lokalnich extremu "Tabu prohledavani", krome toho jsem nasel optimalni velikost tabu pro zadany pocet iteraci. 

Metoda pouziti vetsiho mnozstvi pixelu pro rozeznavani obrazku by take mohla vyzname zrychlit vypocet, ale cilem bylo dosahnout 
lepsich vysledku pro minimalne mozny pocet pixelu.  

