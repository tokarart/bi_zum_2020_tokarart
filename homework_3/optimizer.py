from ChessBoard import Board
from copy import deepcopy


class Optimizer:
    def __init__(self, size, figures, iterations):
        self.board = Board(size)
        self.board.generate_n_queens(figures)
        self.iters = iterations

    def get_neighbours(self):
        """
        method returns list of Board objects with figures made step to random
        position. In new board object only one figure is moved
        """
        res = []
        for f in self.board.figures:
            tmp_board = deepcopy(self.board)
            if tmp_board.move_to_safe_pos(f):
                res.append(tmp_board)
        return res

    def optimize(self):
        """
        steepest hill climbing algorithm
        """
        self.board.draw_board()  # draw initial state
        it = 0
        while self.board.safe_figures() != len(self.board.figures) and it != self.iters:
            neigh_boards = self.get_neighbours()  # get all boards with small changes
            if len(neigh_boards) == 0:
                print('No board neighbours available')
                it += 1
                continue
            best_choice = neigh_boards[0]
            for n in neigh_boards:  # choose best move
                if n.safe_figures() > best_choice.safe_figures():
                    best_choice = n
            if best_choice.safe_figures() > self.board.safe_figures():  # if better than actual board
                self.board = best_choice
                print(self.board.last_move)
            it += 1
        print(f'after {it} iterations ', end='')
        if self.board.safe_figures() == len(self.board.figures):
            print('all figures are in safe')
        else:
            print(f'{self.board.safe_figures()} out of {len(self.board.figures)} are in safe')
        self.board.draw_board()
