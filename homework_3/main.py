from optimizer import Optimizer


def main():
    o = Optimizer(5, 5, 100)  # size_of_board, number_of_queens, iterations for steepest hill climbing
    o.optimize()


if __name__ == '__main__':
    main()
