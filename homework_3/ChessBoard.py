import itertools
from random import randint, choice

import pygame as pg


class Board:
    def __init__(self, size):
        self.size = size
        self.figures = []
        self.last_move = None

    def __random_position__(self):
        """
        returns random position on board
        :return:
        """
        return randint(0, self.size - 1), randint(0, self.size - 1)

    def __check_position_availability__(self, pos) -> bool:
        x, y = pos
        if x < 0 or x >= self.size or y < 0 or y >= self.size:
            return False
        return len([f for f in self.figures if f.pos == pos]) == 0

    def __update_board__(self):
        """
        for each figure updates its status (in_danger/in_safe)
        """
        for f in self.figures:
            f.in_danger = not self.__check_position_safety__(f.pos)

    def __figures_same_line__(self, pos1, pos2):
        """
        check if positions are on same line
        """
        x_1, y_1 = pos1
        x_2, y_2 = pos2
        return (x_1 == x_2) or (y_1 == y_2)

    def __figures_same_diag__(self, pos1, pos2):
        """
        check if figures are on same diagonal
        """
        x_1, y_1 = pos1
        x_2, y_2 = pos2
        return abs(y_1 - y_2) == abs(x_1 - x_2)

    def __desc_diag__(self, pos1, pos2):
        x_1, y_1 = pos1
        for i in range(-7, 8):
            if (x_1 + i, y_1 + i) == pos2:
                return True
        return False

    def __check_jumping(self, pos1, pos2):
        x_1, y_1 = pos1
        x_2, y_2 = pos2

        if self.__figures_same_line__(pos1, pos2):
            if x_1 == x_2:  # vertically
                max_y = max([y_1, y_2])
                min_y = min([y_1, y_2])
                for i in range(min_y + 1, max_y):
                    if not self.__check_position_availability__((x_1, i)):
                        return False
                return True
            else:  # horizontally
                max_x = max([x_1, x_2])
                min_x = min([x_1, x_2])
                for i in range(min_x + 1, max_x):
                    if not self.__check_position_availability__((i, y_1)):
                        return False
                return True
        elif self.__figures_same_diag__(pos1, pos2):
            if self.__desc_diag__(pos1, pos2):  # on descending diagonal
                if y_2 > y_1:
                    hi_pos = pos1
                    lo_pos = pos2
                else:
                    hi_pos = pos2
                    lo_pos = pos1

                hi_x, hi_y = hi_pos
                lo_x, lo_y = lo_pos
                for i in range(1, lo_y - hi_y):
                    if not self.__check_position_availability__((hi_x + i, hi_y + i)):
                        return False
                return True
            else:  # on ascending diagonal
                if y_2 > y_1:
                    hi_pos = pos1
                    lo_pos = pos2
                else:
                    hi_pos = pos2
                    lo_pos = pos1
                hi_x, hi_y = hi_pos
                lo_x, lo_y = lo_pos
                for i in range(1, lo_y - hi_y):
                    if not self.__check_position_availability__((lo_x + i, lo_y - i)):
                        return False
                return True

    def __check_position_safety__(self, pos, except_pos=None):
        """
        check if position safe
        """
        for f in self.figures:
            if pos == f.pos or f.pos == except_pos:
                continue
            if self.__figures_same_diag__(pos, f.pos) or self.__figures_same_line__(pos, f.pos):
                return False
        return True

    def safe_figures(self):
        """
        returns the number of figures in safe
        """
        return len([f for f in self.figures if not f.in_danger])

    def generate_n_queens(self, n):
        if n + len(self.figures) > self.size ** 2:
            print('ERROR: too much figures')
            return

        for i in range(0, n):
            while True:
                x, y = self.__random_position__()
                if self.__check_position_availability__((x, y)):
                    break
            self.figures.append(Queen((x, y)))

        self.__update_board__()

    def get_neighbor_tiles(self, pos):
        """
        returns the list of positions available for move
        """
        x, y = pos

        neighbors = [(x, tmp_y) for tmp_y in range(self.size) if
                     self.__check_position_availability__((x, tmp_y)) and self.__check_jumping(pos, (
                     x, tmp_y))]  # vertical line
        neighbors += [(tmp_x, y) for tmp_x in range(self.size) if
                      self.__check_position_availability__((tmp_x, y)) and self.__check_jumping(pos, (
                      tmp_x, y))]  # horizontal line
        neighbors += [(x + tmp, y + tmp) for tmp in range((-self.size) + 1, self.size) if
                      self.__check_position_availability__((x + tmp, y + tmp)) and self.__check_jumping(pos, (
                      x + tmp, y + tmp))]  # first diag
        neighbors += [(x + tmp, y - tmp) for tmp in range((-self.size) + 1, self.size) if
                      self.__check_position_availability__((x + tmp, y - tmp)) and self.__check_jumping(pos, (
                      x + tmp, y - tmp))]  # second diag

        safe_neighbors = [f for f in neighbors if
                          self.__check_position_safety__(f, pos)]

        return safe_neighbors

    def move_to_safe_pos(self, figure):
        target = None
        for f in self.figures:  # find figure
            if f.pos == figure.pos:
                target = f
                break
        if not target:
            print('Figure not Found')
        safe_neigh = self.get_neighbor_tiles(target.pos)  # list of safe positions
        if len(safe_neigh) == 0:
            # print('No safe positions available')
            return False
        new_pos = choice(safe_neigh)
        self.last_move = target.move(new_pos)
        self.__update_board__()
        return True

    def draw_board(self):

        pg.init()

        BLACK = pg.Color('black')
        WHITE = pg.Color('white')
        RED = pg.Color('red')
        GREEN = pg.Color('green')

        screen = pg.display.set_mode((1080, 1080))
        clock = pg.time.Clock()

        colors = itertools.cycle((WHITE, BLACK))
        tile_size = 40
        width = self.size * tile_size
        background = pg.Surface((width, width))

        for y in range(0, width, tile_size):
            for x in range(0, width, tile_size):
                rect = (x, y, tile_size, tile_size)
                res = [f for f in self.figures if f.pos == (x / tile_size, y / tile_size)]
                if len(res) == 1:
                    if res[0].in_danger:
                        pg.draw.rect(background, RED, rect)  # figure in danger
                    else:
                        pg.draw.rect(background, GREEN, rect)  # figure's safe
                    next(colors)
                else:
                    pg.draw.rect(background, next(colors), rect)
            if self.size % 2 == 0:
                next(colors)

        game_exit = False
        while not game_exit:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    game_exit = True

            screen.fill((60, 70, 90))
            screen.blit(background, (0, 0))

            pg.display.flip()
            clock.tick(30)

        pg.quit()


class Queen:
    def __init__(self, pos):
        self.pos = pos
        self.in_danger = False

    def move(self, pos):
        tmp = f'queen moves from {self.pos} to {pos}'
        self.pos = pos
        return tmp
