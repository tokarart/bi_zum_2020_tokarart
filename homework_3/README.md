# Steepest Hill Climbing algorithm 
Zvolil jsem jako 3. pocitacovou ulohu realizace algoritmu Steepest Hill Climbing pro problem N dam. Program umoznuje udelat sachovnici zvoloene velikosti a nahodne na ni rozmistit N dam. Na zacatku program zobrazi pocatecni stav sachovnice a po zvolenemu poctu iterace zobrazi vysledek. Moje realizace ma obvykly problem Hill Climbingu a zastavuje se na lokalnich extremech. Tento problem jsem neresil. 

## Realizace
Program ma 3 hlavni tridy: Queen(uchovava informace o pozici, dezpecnostni stav), Board(logika presunu kamene, detekce kolizi), Optimizer(vlastni realizace algoritmu a hledani sosusednich situaci)

## Spusteni 
```bash
python3 -m venv ./venv/
source ./venv/bin/activate
pip install -r requirements.txt
```
```bash
python main.py
```